<?php

/**
 * @package         Convert Forms
 * @version         2.7.1 Pro
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2020 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');

extract($displayData);

$readOnly = (isset($field->readonly) && $field->readonly == '1');

if (!$readOnly)
{
	$doc = JFactory::getDocument();

	// Load flatpickr media files
	JHtml::stylesheet('com_convertforms/flatpickr.min.css', ['relative' => true, 'version' => 'auto']);
	JHtml::script('com_convertforms/vendor/flatpickr.js', ['relative' => true, 'version' => 'auto']);

	// Load selected theme
	if ($field->theme)
	{
		JHtml::stylesheet('com_convertforms/flatpickr.' . $field->theme . '.css', ['relative' => true, 'version' => 'auto']);
	}

	// Setup plugin options
	$options = [
		'mode'	     	  => $field->mode,
		'dateFormat' 	  => $field->dateformat,
		'defaultDate'	  => $field->value,
		'minDate'	 	  => $field->mindate,
		'maxDate'	 	  => $field->maxdate,
		'enableTime'	  => (bool) $field->showtimepicker,
		'time_24hr'  	  => (bool) $field->time24,
		'minuteIncrement' => $field->minuteincrement,
		'inline'	 	  => (bool) $field->inline,
		'disableMobile'	  => isset($field->disable_mobile) ? (bool) $field->disable_mobile : false
	];

	// Localize
	$lang = explode('-', \JFactory::getLanguage()->getTag())[0];

	// Grrrrr! Flatpickr doesn't seem to have a strict naming rule for its language files. 
	// A language file can be named either using the ISO 639-1 language code (el) or the ISO 3166-1 country code (gr). 

	// List of locales that need to be converted. 
	$convert_language_codes = [
		'el' => 'gr' // Greek
	];
	if (array_key_exists($lang, $convert_language_codes))
	{
		$lang = $convert_language_codes[$lang];
	}

	// Skip english
	if ($lang != 'en')
	{
		$lang = strtolower($lang);
		$options['locale'] = $lang;
		$doc->addScriptDeclaration('flatpickr.localize(flatpickr.l10ns.' . $lang . ');');
		$doc->addScript('//npmcdn.com/flatpickr/dist/l10n/' . $lang . '.js');
	}

	// Setup plugin
	// Note: we are targeting the input field with a full element selector input[id='INPUT_ID'] instead of the simple #id selector
	// to prevent DOM invalid selector error caused when the ID contains spaces and special characters.
	$doc->addScriptDeclaration('
		document.addEventListener("DOMContentLoaded", function(event) { 
			flatpickr.l10ns.default.firstDayOfWeek = ' . (isset($field->firstDayOfWeek) ? $field->firstDayOfWeek : 1) . ';
			flatpickr("input[id=\'' . $field->id . '\']", ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK) . ');

			// Fix the appearance of native date picker to match the design of the rest of the inputs.
			var mobile_input = document.querySelector(".flatpickr-mobile");
			if (mobile_input) {
				mobile_input.setAttribute("style", mobile_input.previousSibling.getAttribute("style"));
			}
		});
	');

	// Since the rangeSeparator string is different per language, we need to pass its value to the form
	// submission in order to properly validate the 2 dates. 
	// This is required only if the Date Range mode is enabled and we are using a non-english locale
	if ($field->mode == 'range' && $lang != 'en')
	{
		$doc->addScriptDeclaration('
			document.addEventListener("DOMContentLoaded", function(event) { 
				var fp = document.querySelector("#' . $field->id .'");

				if (fp) {
					fp._flatpickr.set("onChange", function(selectedDates, dateStr, instance) {
						var range_separator = instance.l10n.rangeSeparator;
			
						if (!instance.element.classList.contains("range_separator")) {
							var spr_el = document.createElement("input");
							spr_el.setAttribute("type", "hidden");
							spr_el.setAttribute("name", "' . $field->key . '_rs");
							spr_el.value = range_separator;
					
							instance.element.closest(".cf-control-input").appendChild(spr_el);    
							instance.element.className += " range_separator";
						}
					})
				}
			});
		');
	}

	$doc->addStyleDeclaration('
		.flatpickr-current-month .flatpickr-monthDropdown-months{
			height: auto !important;
			display: inline-block !important;
		}
		.flatpickr-current-month .flatpickr-monthDropdown-months:hover {
			background: none;
		}
		.numInputWrapper {
			margin-left: 10px;
		}
		.numInputWrapper:hover {
			background: none;
		}
		.flatpickr-calendar input {
			box-shadow: none !important;
		}
		.flatpickr-calendar .flatpickr-time input {
			height: auto !important;
			border: none !important;
			box-shadow: none !important;
			font-size: 16px !important;
			margin: 0 !important;
			padding: 0 !important;
			line-height: inherit !important;
			background: none !important;
			color: ' . ($field->theme == "dark" ? "#fff" : "#484848")  . ' !important;
		}
		.flatpickr-calendar.inline {
			margin-top:5px;
		}
		.flatpickr-calendar.open {
			z-index: 99999999; // EngageBox v4 uses z-index: 99999999;
		}
		.flatpickr-mobile {
			-webkit-appearance: textfield; // Remove iOS ugly button
			-moz-appearance: textfield;
		}
		.flatpickr-calendar .numInputWrapper .cur-year {
			height: auto !important;
		}
	');
}

?>

<input type="text" name="<?php echo $field->name ?>" id="<?php echo $field->id; ?>"
	<?php if (isset($field->required) && $field->required) { ?>
		required
	<?php } ?>

	<?php if (isset($field->placeholder) && !empty($field->placeholder)) { ?>
		placeholder="<?php echo htmlspecialchars($field->placeholder, ENT_COMPAT, 'UTF-8'); ?>"
	<?php } ?>

	<?php if (isset($field->value) && !empty($field->value)) { ?>
		value="<?php echo $field->value; ?>"
	<?php } ?>

	<?php if ($readOnly) { ?>
		readonly
	<?php } ?>

	autocomplete="off"
	class="<?php echo $field->class ?>"
	style="<?php echo $field->style; ?>"
>