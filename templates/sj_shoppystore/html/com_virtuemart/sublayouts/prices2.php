<?php
	/**
	 *
	 * Show the product prices
	 *
	 * @package    VirtueMart
	 * @subpackage
	 * @author Max Milbers, Valerie Isaksen
	 * @link http://www.virtuemart.net
	 * @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
	 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
	 * VirtueMart is free software. This version may have been modified pursuant
	 * to the GNU General Public License, and as distributed it includes or
	 * is derivative of works licensed under the GNU General Public License or
	 * other free or open source software licenses.
	 * @version $Id: default_showprices.php 8024 2014-06-12 15:08:59Z Milbo $
	 */
	// Check to ensure this file is included in Joomla!
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	
	if(
		isset($_POST['email']) && $_POST['email'] &&
		isset($_POST['name']) && $_POST['name'] &&
		isset($_POST['sku']) && $_POST['sku'] &&
		isset($_POST['product_name']) && $_POST['product_name']
	)
	{
		$myUser = new stdClass();
		$myUser->email = $_POST['email'];
		$myUser->name = $_POST['email'];
		$myUser->confirmed = 1;
		$customFields = [];
		$customFields['5'] = $_POST['name'];
		$customFields['4'] = $_POST['sku'];
		$customFields['6'] = $_POST['product_name'];
		$userClass = acym_get('class.user');
		$userClass->sendConf = true;
		$userId = $userClass->save($myUser, $customFields);
		
	}
?>
<div class="product-price" id="productPrice<?php echo $product->virtuemart_product_id ?>">
	<?php
		if (!empty($product->prices['salesPrice']))
		{
			//echo '<div class="vm-cart-price">' . vmText::_('Giá bán: ') . '</div>';
		}

		if ($product->prices['salesPrice'] <= 0 and VmConfig::get('askprice', 1) and isset($product->images[0]) and !$product->images[0]->file_is_downloadable)
		{
			$askquestion_url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&task=askquestion&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id . '&tmpl=component', FALSE);
			
			if(!include_once(rtrim(JPATH_ADMINISTRATOR, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_acym'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php'))
			{
				echo 'This code can not work without the AcyMailing Component';
				return false;
			}
			
			?>
            <form action="#" method="POST">
                <div class="form-group">
                    <label for="ten">Tên:</label>
                    <input name="name" type="text" class="form-control" id="name" placeholder="Tên: ">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="nhập email">
                </div>
                <div class="form-group">
                    <label for="sodienthoai">Số Điện Thoại:</label>
                    <input type="text" class="form-control" id="phone" placeholder="Số Điện Thoại: ">
                </div>
                <div class="form-group">
                    <label for="sku">Mã SKU</label>
                    <input name="sku" type="text" value="<?= $product->product_sku ?>" class="form-control" id="product_sku" >
                </div>
                <div class="form-group">
                    <label for="sku">Tên Sản Phẩm</label>
                    <input name="product_name" type="text" value="<?= $product->product_name ?>" class="form-control" id="product_name" >
                </div>
                <button type="submit" class="btn btn-primary">Gởi</button>
            </form>
			<?php
		}
		else
		{
			echo $currency->createPriceDiv('basePrice', 'COM_VIRTUEMART_PRODUCT_BASEPRICE', $product->prices);

			echo $currency->createPriceDiv('basePriceVariant', 'COM_VIRTUEMART_PRODUCT_BASEPRICE_VARIANT', $product->prices);
			
			echo $currency->createPriceDiv('variantModification', 'COM_VIRTUEMART_PRODUCT_VARIANT_MOD', $product->prices);

			if (round($product->prices['basePriceWithTax'], $currency->_priceConfig['salesPrice'][1]) != round($product->prices['salesPrice'], $currency->_priceConfig['salesPrice'][1]))
			{
				echo '<span class="price-crossed" >' . $currency->createPriceDiv('basePriceWithTax', 'COM_VIRTUEMART_PRODUCT_BASEPRICE_WITHTAX', $product->prices) . "</span>";
			}
			
			if (round($product->prices['salesPriceWithDiscount'], $currency->_priceConfig['salesPrice'][1]) != round($product->prices['salesPrice'], $currency->_priceConfig['salesPrice'][1]))
			{
				echo $currency->createPriceDiv('salesPriceWithDiscount', 'COM_VIRTUEMART_PRODUCT_SALESPRICE_WITH_DISCOUNT', $product->prices);
			}

			echo $currency->createPriceDiv('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices);

			if ($product->prices['discountedPriceWithoutTax'] != $product->prices['priceWithoutTax'])
			{
				echo $currency->createPriceDiv('discountedPriceWithoutTax', 'COM_VIRTUEMART_PRODUCT_SALESPRICE_WITHOUT_TAX', $product->prices);
			}
			else
			{
				echo $currency->createPriceDiv('priceWithoutTax', 'COM_VIRTUEMART_PRODUCT_SALESPRICE_WITHOUT_TAX', $product->prices);
			}
			
			
			
//			echo $currency->createPriceDiv('discountAmount', 'COM_VIRTUEMART_PRODUCT_DISCOUNT_AMOUNT', $product->prices);
			
			echo $currency->createPriceDiv('taxAmount', 'COM_VIRTUEMART_PRODUCT_TAX_AMOUNT', $product->prices);

			$unitPriceDescription = vmText::sprintf('COM_VIRTUEMART_PRODUCT_UNITPRICE', vmText::_('COM_VIRTUEMART_UNIT_SYMBOL_' . $product->product_unit));

			echo $currency->createPriceDiv('unitPrice', $unitPriceDescription, $product->prices);
		}
	?>
</div>

