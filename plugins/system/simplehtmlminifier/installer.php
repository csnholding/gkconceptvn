<?php

defined('_JEXEC') or die('Restricted Access');

/* Simple HTML Minifier extension for Joomla!
--------------------------------------------------------------
 Copyright (C) 2016 AddonDev. All rights reserved.
 Website: https://addondev.com
 GitHub: https://github.com/addondev
 Developer: Philip Sorokin
 Location: Russia, Moscow
 E-mail: philip.sorokin@gmail.com
 Created: January 2016
 License: GNU GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
--------------------------------------------------------------- */

class PlgSystemSimplehtmlminifierInstallerScript
{
	public function preflight($type, $parent)
	{
		$app = JFactory::getApplication();
		$dbo = JFactory::getDbo();
		
		$minJVersion = '3.6.5';
		$minPHPVersion = '5.4';
		
		if(version_compare(JVERSION, $minJVersion, '<'))
		{
			$app->enqueueMessage(JText::sprintf('PLG_SIMPLEHTMLMINIFIER_JOOMLA_VERSION_CHECK_FAILURE', $minJVersion), 'error');
			return false;
		}
		
		if(version_compare(PHP_VERSION, $minPHPVersion, '<'))
		{
			$app->enqueueMessage(JText::sprintf('PLG_SIMPLEHTMLMINIFIER_PHP_VERSION_CHECK_FAILURE', $minPHPVersion), 'error');
			return false;
		}
		
		$manifest = $parent->get('manifest');
		$update_server = (string) $manifest->updateservers->server;
		$old_update_server = 'http://addondev.com/support/updates/simplehtmlcompressor.xml';
		
		if($update_server)
		{	
			$dbo->setQuery($dbo->getQuery(true)
				->SELECT('update_site_id')
				->FROM($dbo->quoteName('#__update_sites'))
				->WHERE($dbo->quoteName('location') . ' LIKE ' . $dbo->quote($old_update_server . '%'))
			);
			
			if($update_ids = $dbo->loadObjectList())
			{
				foreach($update_ids as &$element)
				{
					$element = (int) $element->update_site_id;
				}
				
				$update_ids = implode(',', $update_ids);
				
				$dbo->setQuery($dbo->getQuery(true)
					->DELETE($dbo->quoteName('#__update_sites'))
					->WHERE($dbo->quoteName('update_site_id') . ' IN (' . $update_ids . ')')
				);
				$dbo->execute();
				
				$dbo->setQuery($dbo->getQuery(true)
					->DELETE($dbo->quoteName('#__update_sites_extensions'))
					->WHERE($dbo->quoteName('update_site_id') . ' IN (' . $update_ids . ')')
				);
				$dbo->execute();
				
				$dbo->setQuery($dbo->getQuery(true)
					->DELETE($dbo->quoteName('#__updates'))
					->WHERE($dbo->quoteName('update_site_id') . ' IN (' . $update_ids . ')')
				);
				$dbo->execute();
			}
		}
	}
	
	public function postflight($type, $parent)
	{
		$dbo = JFactory::getDbo();
		
		$oldPlugin = $dbo->setQuery($dbo->getQuery(true)
			->SELECT($dbo->qn(['extension_id', 'params', 'enabled']))
			->FROM('#__extensions')
			->WHERE($dbo->qn('name') . ' = ' . $dbo->q('plg_system_simplehtmlcompressor'))
		)->loadObject();
		
		if ($oldPlugin)
		{
			$parent->uninstall($oldPlugin->extension_id);
			
			$dbo->setQuery($dbo->getQuery(true)
				->update('#__extensions')
				->set([
					$dbo->qn('params') . ' = ' . $dbo->q($oldPlugin->params),
					$dbo->qn('enabled') . ' = ' . (int) $oldPlugin->enabled
				])
				->where($dbo->qn('element') . ' = ' . $dbo->q('simplehtmlminifier'))
			)->execute();			
		}
	}
}
