<?php
/**
 * @version        1.0
 * @package        DJ Img To Webp
 * @copyright    Copyright (C) 2019 DJ-Extensions.com LTD, All rights reserved.
 * @license        http://www.gnu.org/licenses GNU/GPL
 * @author        url: http://design-joomla.eu
 * @author        email contact@design-joomla.eu
 * @developer    Mateusz Maciejewski - mateusz.maciejewski@indicoweb.com
 *
 * You should have received a copy of the GNU General Public License.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

defined('_JEXEC') or die();
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

use Joomla\Registry\Registry;

defined('_JEXEC') or die();
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');


class JFormFieldDJMenuItems extends JFormField
{

    protected $type = 'DJMenuItems';

    protected function getInput()
    {
        $app = JFactory::getApplication();

        $id = isset($this->element['id']) ? $this->element['id'] : null;
        $cssId = '#' . $this->getId($id, $this->element['name']);

        $limit = $this->multiple ? 0 : 1;

        $urlVars = 'index.php?option=com_ajax&group=system&plugin=DJImgToWebp&format=json';
        $url = JUri::root() . $urlVars;

        $chosenAjaxSettings = new Registry(
            array(
                'selector' => $cssId,
                'type' => 'GET',
                'url' => $url,
                'dataType' => 'json',
                'jsonTermKey' => 'like',
                'minTermLength' => 2,
                'disable_search_threshold' => 20
            )
        );

        $chosenSettings = array();
        if ($limit > 0) {
            $chosenSettings['max_selected_options'] = (int)$limit;
        }
        $chosenSettings['disable_search_threshold'] = 0;
        $chosenSettings['placeholder_text_multiple'] = JText::_('PLG_SYSTEM_DJIMGTOWEBP_MENU_ITEMS_PLACEHOLDER');
        $chosenSettings['placeholder_text_single'] = JText::_('PLG_SYSTEM_DJIMGTOWEBP_MENU_ITEM_PLACEHOLDER');
        $chosenSettings = new Registry($chosenSettings);

        JHtml::_('formbehavior.chosen', $cssId, null, $chosenSettings);
        JHtml::_('formbehavior.ajaxchosen', $chosenAjaxSettings);

        // Initialize some field attributes.
        $size = $this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '';
        $maxLength = $this->element['maxlength'] ? ' maxlength="' . (int)$this->element['maxlength'] . '"' : '';
        $attr = '';

        $class = $this->element['class'] ? (string)$this->element['class'] : '';
        $class .= $this->element['required'] == 'true' ? ' required' : '';

        $attr .= 'class="' . $class . '"';
        $name = $this->name;
        if (isset($this->element['multiple']) && $this->element['multiple'] == 'true') {
            $attr .= ' multiple="true" size="10"';
        } else {
            // such situations should not happen, but if they do (it's possible) we need to assign at least one group of fields
            if (is_array($this->value)) {
                $newValue = 0;
                foreach ($this->value as $v) {
                    if ($v > 0) {
                        $newValue = $v;
                        break;
                    }
                }
                $this->value = $newValue;
            }
        }

        if ($this->onchange != '') {
            $attr .= ' onchange="' . trim($this->onchange) . '"';
        }


        $options = array();

        if (!isset($this->element['multiple']) || $this->element['multiple'] != 'true') {
            $options[] = JHTML::_('select.option', '', JText::_('PLG_SYSTEM_DJIMGTOWEBP_MENU_ITEMS_PLEASE_SELECT'));
        }

        $options[] = JHTML::_('select.option', 0, JText::_('PLG_SYSTEM_DJIMGTOWEBP_MENU_ITEMS_NONE'));

        $selected = array();


        if (!empty($this->value)) {
            $menuItems = $this->getMenuItems($this->value);

            foreach ($menuItems as $menuItem) {
                if (in_array($menuItem->value, $this->value) || $this->value == 0) {
                    $selected[] = $menuItem->text;
                    $options[] = JHtml::_('select.option', $menuItem->value, $menuItem->text);
                }
            }

        }

        $out = JHtml::_('select.genericlist', $options, $name, trim($attr), 'value', 'text', $this->value, $this->id);

        return ($out);
    }

    private function getMenuItems($pk)
    {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('m.id AS value, m.title AS text')
            ->from($db->quoteName('#__menu', 'm'))
            ->where('m.menutype <> ' . $db->quote('main'))
            ->where('m.published = 1');

        if (is_array($pk)) {
            $query->where('m.id IN (' . implode($pk, ',') . ')');
        } else {
            $query->where('m.id = ' . (int)$pk);
        }

        $db->setQuery($query);
        return $db->loadObjectList();
    }
}

?>