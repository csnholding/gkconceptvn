<?php
/**
 * @version		1.0
 * @package		DJ Img To Webp
 * @copyright 	Copyright (C) 2019 DJ-Extensions.com LTD, All rights reserved.
 * @license 		http://www.gnu.org/licenses GNU/GPL
 * @author 		url: http://design-joomla.eu
 * @author 		email contact@design-joomla.eu
 * @developer 	Mateusz Maciejewski - mateusz.maciejewski@indicoweb.com
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ Classifieds. If not, see <http://www.gnu.org/licenses/>.
 *
 */

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');

require_once JPATH_ROOT.'/plugins/system/djimgtowebp/helper.php';
require_once JPATH_ROOT.'/plugins/system/djimgtowebp/lib/autoload.php';

use WebPConvert\WebPConvert;

class PlgSystemDJImgToWebp extends JPlugin
{
    protected $_webps;

    function onAfterRender()
    {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();


        if (JFactory::getDocument()->getType() !== 'html' || $app->isAdmin()) {
            return;
        }

        $gdInfo = gd_info();
        if(!isset($gdInfo['WebP Support']) OR !$gdInfo['WebP Support']) return;

        if($this->menuItemIsExcluded()) return;

        if (DJWebPHelper::browserSupportWebp()) {
            $sHtml = $app->getBody();


            $filters = json_decode($this->params->get('filters'));

            $webp_purge = $app->input->get('webp_purge', false, 'bool');
            $purge = false;
            if ($webp_purge && $user->authorise('core.admin')) {
                $purge = true;
            }

            $debugData = array();

            if (isset($filters->directory) && count($filters->directory)) {
                foreach ($filters->directory as $index => $directory) {

                    $extensions = isset($filters->extensions[$index]) ? (array) $filters->extensions[$index] : array();
                    $quality = isset($filters->quality[$index]) ? $filters->quality[$index] : 100;
                    $stored_time = isset($filters->stored_time[$index]) ? $filters->stored_time[$index] : 5;
                    $excluded = isset($filters->excluded[$index]) ? $filters->excluded[$index] : '';
                    $excludedArr = strlen($excluded) ? explode(';', $excluded) : array();

                    $debugData[$directory] = array();
                    $debugTarget =  &$debugData[$directory];

                    if (substr($directory, 0, 1) === '/') {
                        $directory = substr($directory, 1);
                    }

                    if (substr($directory, -1) === '/') {
                        $directory = substr($directory, 0, -1);
                    }

                    if(count($extensions)) {
                        $regexPath = str_replace("/", "\/", $directory);
                        $sHtml = preg_replace_callback(
                            '/' . $regexPath . '\/\S+(?:' . implode( '|',$extensions) . ')\b/',
                            function ($match) use ($quality, $stored_time, $excludedArr,  $purge, &$debugTarget, $regexPath) {
                                $img = $match[0];



                                $newImg = $this->imgToWebp($img, $quality, $excludedArr,  $stored_time, $purge, $regexPath, $match);

                                $debugTarget[] = array(
                                    'source' => $img,
                                    'target' => $newImg
                                );

                                return $newImg ? $newImg : $img;
                            },
                            $sHtml
                        );
                    }


                }
            }

            if($this->params->get('debug')) {
                $sHtml .= '<pre>' . print_r($debugData, true) . '</pre>';
            }


            $app->setBody($sHtml);
        }
    }

    private function menuItemIsExcluded() {
        $app  = JFactory::getApplication();


        $excludedMenuItems = $this->params->get('excludedMenus', array(), 'array');
        $activeMenu = $app->getMenu()->getActive();

        if(isset($activeMenu->id))
            return in_array($activeMenu->id, $excludedMenuItems);
        else
            return false;
    }

    private function isExcludedDirectory($image, $excluded) {
        $exist = false;
        foreach ($excluded as $exclude) {

            if( strpos( $image, $exclude ) !== false) {
                $exist = true;
                break;
            }
        }
        return $exist;
    }

    private function imgToWebp($image, $quality = 100, $excluded = array(), $stored_time = 5, $purgePrevious = false, $regexPath = '', $fullRegex = '')
    {
        $imgPath = JPATH_ROOT . '/' . $image;
        $imgInfo = pathinfo($imgPath);
        $imgHash = md5($imgPath);


        if(!isset($imgInfo['extension']) || !$imgInfo['extension']) return;

        if(count($excluded)) {
            if(in_array($image, $excluded) || $this->isExcludedDirectory($image, $excluded)) {
                return;
            }
        }

        if(JFile::exists($imgPath)) {
            if (!isset($this->_webps[$imgHash])) {
                $newImagePath = $imgInfo['dirname'] . '/';
                $newImage = $newImagePath . $imgInfo['filename'] . '.webp';


                // Delete webp image if is older then 5 hours

                if (JFile::exists($newImage)) {
                    $fileCreated = filemtime($newImage);
                    if($fileCreated && (((time() - $fileCreated) / 3600) >= $stored_time || $purgePrevious)) {
                        JFile::delete($newImage);
                    }
                }

                if (!JFile::exists($newImage)) {

                    if (JFile::exists($imgPath)) {

                        if($this->params->get('mode') == '') {
                            switch (strtolower($imgInfo['extension'])) {
                                case 'png' :
                                    $img = imagecreatefrompng($imgPath);
                                    break;
                                case 'jpg' :
                                case 'jpeg' :
                                    $img = imagecreatefromjpeg($imgPath);
                            }

                            if (!JFolder::exists($newImagePath)) {
                                JFolder::create($newImagePath);
                            }

                            imagepalettetotruecolor($img);
                            imagealphablending($img, true);
                            imagesavealpha($img, true);
                            imagewebp($img, $newImage, $quality);
                        } elseif ($this->params->get('mode') == 'library' ) {
                            $options = array(
                                'converters' => array(
                                    $this->params->get('method', 'gd')
                                )
                            );
                            WebPConvert::convert($imgPath, $newImage, $options);
                        }


                    }
                }

                $newFile = str_replace(JPATH_ROOT . '/', "", $newImage);
                $this->_webps[$imgHash] = $newFile;

            }
        }




        return $this->_webps[$imgHash];
    }

    public function onAjaxDJImgToWebp() {
        $app = JFactory::getApplication();

        $like = trim($app->input->get('like', null, 'string'));

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('m.id AS value, m.title AS text')
            ->from($db->quoteName('#__menu', 'm'))
            ->where($db->quoteName('m.title') . ' LIKE ' . $db->quote('%' . $db->escape($like) . '%'))
            ->where('m.menutype <> ' . $db->quote('main'))
            ->where('m.published = 1');

        $query->order('title ASC');
        $db->setQuery($query);
        $results = $db->loadObjectList();

        echo json_encode($results);

        $app->close();
    }
}
