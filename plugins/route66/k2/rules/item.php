<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleK2Item extends Route66Rule
{
	private static $cache = array();
	protected $variables = array('option' => 'com_k2', 'view' => 'item', 'id' => '@');

	public function getTokensValues($query)
	{
		// Cache key
		$key = (int) $query['id'];

		// Check cache
		if (isset(self::$cache[$key]))
		{
			return self::$cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{itemId}')
			{
				$values[] = (int) $query['id'];
				$dbQuery->select($db->qn('item.id'));
			}
			// Alias
			elseif ($token == '{itemAlias}')
			{
				if (strpos($query['id'], ':'))
				{
					$parts = explode(':', $query['id']);
					$values[] = $parts[1];
				}
				$dbQuery->select($db->qn('item.alias'));
			}
			// Category alias
			elseif ($token == '{categoryAlias}')
			{
				$dbQuery->select($db->qn('category.alias'));
			}
			// Category path
			elseif ($token == '{categoryPath}')
			{
				$dbQuery->select($db->qn('category.id'));
			}
			// Item year
			elseif ($token == '{itemYear}')
			{
				$dbQuery->select('YEAR(' . $db->qn('item.created') . ')');
			}
			// Item month
			elseif ($token == '{itemMonth}')
			{
				$dbQuery->select('DATE_FORMAT(' . $db->qn('item.created') . ', "%m")');
			}
			// Item day
			elseif ($token == '{itemDay}')
			{
				$dbQuery->select('DATE_FORMAT(' . $db->qn('item.created') . ', "%d")');
			}
			// Item date
			elseif ($token == '{itemDate}')
			{
				$dbQuery->select('DATE(' . $db->qn('item.created') . ')');
			}
			// Item author
			elseif ($token == '{itemAuthor}')
			{
				$dbQuery->select('CASE WHEN ' . $db->qn('item.created_by_alias') . ' = ' . $db->q('') . ' THEN ' . $db->qn('item.created_by') . ' ELSE ' . $db->qn('item.created_by_alias') . ' END ');
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			self::$cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__k2_items', 'item'));
		$dbQuery->innerJoin($db->qn('#__k2_categories', 'category') . ' ON ' . $db->qn('item.catid') . ' = ' . $db->qn('category.id'));
		$dbQuery->where($db->qn('item.id') . ' = ' . (int) $query['id']);
		$db->setQuery($dbQuery);
		$values = $db->loadRow();

		// Some values need processing
		$author = array_search('{itemAuthor}', $this->tokens);

		if ($author !== false)
		{
			if (is_numeric($values[$author]))
			{
				$values[$author] = JFactory::getUser($values[$author])->name;
			}
			$values[$author] = JFilterOutput::stringURLUnicodeSlug($values[$author]);
		}
		$categoryPath = array_search('{categoryPath}', $this->tokens);

		if ($categoryPath !== false)
		{
			JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/k2/models');
			$model = JModelLegacy::getInstance('K2', 'Route66Model', array('ignore_request' => true));
			$category = $model->getCategory($values[$categoryPath]);
			$values[$categoryPath] = (int) $category->parent ? $model->getCategoryPath($category->id, $category->parent, array($category->alias)) : $category->alias;
		}
		self::$cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'id')
		{
			// First check that ID is not already in the URL
			if (isset($tokens['{itemId}']))
			{
				return $tokens['{itemId}'];
			}

			// Check for alias
			if (isset($tokens['{itemAlias}']))
			{
				return $this->getItemIdFromAlias($tokens['{itemAlias}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('id'))->select($db->qn('catid'))->select($db->qn('language'))->from($db->qn('#__k2_items'))->where($db->qn('id') . ' = ' . $db->q($variables['id']));
		$db->setQuery($query);
		$item = $db->loadObject();

		$route = K2HelperRoute::getItemRoute($item->id, $item->catid);
		parse_str($route, $result);
		$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

		if (!$Itemid)
		{
			require_once JPATH_SITE . '/plugins/route66/k2/helpers/route.php';
			$route = Route66K2HelperRoute::getItemRoute($item->id, $item->catid, $item->language);
			parse_str($route, $result);
			$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';
		}

		return $Itemid;
	}

	private function getItemIdFromAlias($alias)
	{
		if (strpos($alias, '/') !== false)
		{
			$parts = explode('/', $alias);
			$alias = end($parts);
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')->from('#__k2_items')->where('alias = ' . $db->q($alias));
		$application = JFactory::getApplication();

		if ($application->isClient('site') && $application->getLanguageFilter())
		{
			$query->where($db->qn('language') . ' IN(' . $db->q('*') . ', ' . $db->q($this->getLanguage()) . ')');
		}
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}
}
